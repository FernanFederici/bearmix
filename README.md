# BEARmix: **B**asic **E**conomical **A**mplification **R**eaction one-step RT-qPCR master mix
Thomas G.W. Graham, Gina M. Dailey, Claire Dugast-Darzacq and Meagan N. Esbin; manuscript in preparation

An open-source, one-step RT-qPCR master mix that can be assembled using inexpensive
buffer components, homemade M-MLV reverse transcriptase, and hot-start Taq
polymerase. It permits the reliable detection of small numbers of RNA molecules.

Taq polymerase and M-MLV reverse transcriptase expression plasmid sequences are provided as .dna files.

## Disclaimer

This protocol is not yet FDA-approved for clinical diagnostic use and
is provided for research purposes only.

## Contact

Please direct questions and plasmid requests to TDbearmix@gmail.com.
